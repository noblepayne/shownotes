(ns shownotes.core
  (:require [reagent.core :as r]
            [reagent.dom]
            [reagent.dom.server]
            [cljsjs.react-bootstrap]
            [goog.dom :as dom]
            [clojure.spec :as s]
            [clojure.string :as str]
            [clojure.set :refer [difference] :rename {difference set-diff}]
            [cljs.core.match :refer-macros [match]]))

(enable-console-print!)

(comment  ;; use clojure-mode
  (use '[figwheel-sidecar.repl-api])
  (start-figwheel!)
  (cljs-repl)
)

(defonce app-state (r/atom {:text "enter text"
                            :ta-text nil
                            :link-input-toggle :inline
                            :buffer-form-toggle :none
                            :buffers [{:name "blank buffer"
                                       :links []}]
                            :_counter 0}))

(def Button  (aget js/ReactBootstrap "Button"))

;; specs
(s/def ::string string?)

(s/def ::url
  (s/and ::string
         #(re-matches #"^https?://.*\..*" %)))

(s/def ::note
  (s/cat :title ::string
         :url   ::url))

(s/def ::notes
  (s/* ::note))

(s/def ::buffer
  (s/cat :name ::string
         :links ::notes))

(s/def ::buffers
  (s/* ::buffer))


;; fns
(defn get-event-value
  "Return .value from event target."
  [event]
  (-> event
      .-currentTarget
      .-value))

(defn get-link-id-from-event
  "Return idx data attribute from link element."
  [event]
  (-> event
             .-currentTarget
             .-dataset
             .-idx
             js/parseInt))

(defn save-text-value
  "Save text field value in app-state at :kywd."
  [kywd]
  (fn [e]
    (swap! app-state
           update-in [kywd]
           #(get-event-value e))))

(defn display-toggle
  "Switch visibility of an element."
  [key-name]
  (fn []
    (swap! app-state
           update-in [key-name]
           #(first (set-diff #{:none :inline} #{%})))))

(defn vec-remove
  "remove the nth element of v"
  [n v]
  (if (> (count v) n)
    (vec (concat
          (subvec v 0 n)
          (subvec v (inc n))))
    v))


(defn create-buffer! []
  (let [buffer-input (dom/getElement "buffername")
        buffer-name (.-value buffer-input)]
    (swap! app-state
           update-in [:buffers]
           conj {:name buffer-name :links []})
    (set! (.-value buffer-input) "")
    ((display-toggle :buffer-form-toggle))))

(defn remove-buffer! [buffidx]
  (swap! app-state
         update-in [:buffers]
         (partial vec-remove buffidx)))

(defn swap
  "Given vector v swaps elements at indexes n and m.
  Returns new vector with elements swapped or same vector
  if arguments are out-of-bounds."
  [v n m]
  (let [l (count v)]
    (cond (and (>= n 0)
               (>= m 0)
               (< n l)
               (< m l))
            (assoc v
                   n (v m)
                   m (v n))
          :else v)))

(defn move-
  "Move nth item of list li down one."
  [n li]
  (swap li n (- n 1)))

(defn move+
  "Move nth item of list li up one."
  [n li]
  (swap li n (+ n 1)))


(defn load-links!
  "Load parsed links into :links in app-state."
  [buffidx]
  (let [el (dom/getElement "link-input")
        notes (filter not-empty (str/split (.-value el) "\n"))
        c-notes (s/conform ::notes notes)]
    (case c-notes
      :cljs.spec/invalid (println (s/explain ::notes notes))
      (do
        (swap! app-state update-in [:buffers buffidx :links] (fn [] c-notes))
        ((display-toggle :link-input-toggle))))))

(defn move-link
  "Handler function to move link w/ provided move fn."
  [move-fn buffer]
  (fn [e]
    (let [id (get-link-id-from-event e)]
      (swap! app-state
             update-in [:buffers buffer :links]
             (partial move-fn id)))))


(defn remove-link [buffer link-n]
  (swap! app-state
         update-in [:buffers buffer :links]
         (partial vec-remove link-n)))

(defn switch-link-buffer [update-fn buffidx idx]
  (let [link (-> @app-state :buffers (nth buffidx) :links (nth idx))
        bufflen (count (-> @app-state :buffers))
        new-buffidx (mod (update-fn buffidx) bufflen)]
    (swap! app-state update-in [:buffers new-buffidx :links] conj link)
    (remove-link buffidx idx)))

(defn unform-links [links]
  (->> links
       (map (partial s/unform ::note))
       (map (partial str/join "\n"))
       (str/join "\n\n")))

(defn download [filename, text]
  (let [element (.createElement js/document "a")]
    (doto element
      (.setAttribute "href"
                     (str "data:text/plain;charset=utf-8,"
                          (js/encodeURIComponent text)))
      (.setAttribute "download"
                     filename))
    (set! (.-style.display element) "none")
    (.appendChild js/document.body element)
    (.click element)
    (.removeChild js/document.body element)))

(defn download-links [buffname buffidx]
  (let [links (-> @app-state :buffers (nth buffidx) :links)
        output (unform-links links)]
    (download (:name buffname) output)))

;; components
(defn main-control
  "Main app control panel."
  []
  [:div
   ;; [:button {:on-click (partial load-links! 0)} "Load Links"]
   [:button {:on-click (display-toggle :link-input-toggle)} "Load Links"]
   [:> Button {:on-click (display-toggle :buffer-form-toggle)} "New Buffer"]
   [:span.toggler {:style {:padding-left "8px" :display (:buffer-form-toggle @app-state)}}
    [:input#buffername {:style {:padding "2px"}}]
    [:> Button {:on-click create-buffer!} "create"]]])

(defn link->div
  "Individual link element. Takes link map and index position."
  [link idx buffidx]
  [:div.linkdiv
   [:span.uplink
    {:data-idx idx
     :data-buffidx buffidx
     :on-click (move-link move- buffidx)}
    "UP"]
   [:span.downlink
    {:data-idx idx
     :data-buffidx buffidx
     :on-click (move-link move+ buffidx)}
    "DOWN"]
   [:span.buffplus
    {:data-idx idx
     :data-buffidx buffidx
     :on-click (partial switch-link-buffer inc buffidx idx)}
    "B+"]
   [:span.buffminus
    {:data-idx idx
     :data-buffidx buffidx
     :on-click (partial switch-link-buffer dec buffidx idx)}
    "B-"]
   [:span.remove
    {:data-idx idx
     :data-buffidx buffidx
     :on-click (partial remove-link buffidx idx)}
    "X"]
   [:a.linkbox {:href (:url link)} (:title link)]])

(defn link-input
  "Link input textarea."
  []
  [:div {:style {:display (:link-input-toggle @app-state)}}
   [:textarea#link-input
    {:style         {:width 500 :height 300}
     :default-value (:ta-text @app-state)
     :on-change     (save-text-value :ta-text)}]
   [:> Button {:on-click (partial load-links! 0) :style {:display :block}} "load!"]])

(defn buffer
  "Individual buffer components."
  [buff idx]
  [:div.buffer {:data-idx idx}
   [:p
    [:span.buffclose {:on-click #(remove-buffer! idx)} "X"]
    [:span.buffdl {:on-click #(download-links buff idx)} "DL"]
    (:name buff)]
   (for [[id link] (map-indexed vector (:links buff))]
     ^{:key id} [link->div link id idx])])

(defn main-comp
  "Main component of app."
  []
  [:div
   [main-control]
   [link-input]
   (for [[idx buff] (map-indexed vector (:buffers @app-state))]
     ^{:key idx} [buffer buff idx])])


;; App Lifecycle
(defn mount-app []
  (r/render [main-comp] (.getElementById js/document "app")))

(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
  (swap! app-state update-in [:_counter] inc)
)

(mount-app)
